from .script import (
     process
)

__title__ = 'translig'
__version__ = '0.0.1'
__author__ = 'Giga Chokheli'
__copyright__ = '2018 Giga Chokheli'
__license__ = 'GPL 2.0/LGPL 2.1'
__all__ = (
 'process'
 )