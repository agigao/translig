# coding: utf-8
import os
import random
import sys

from transliterate.base import TranslitLanguagePack, registry
from transliterate import translit


class VB(TranslitLanguagePack):
    """Class for mapping alien chars to Georgian counterparts."""
    language_code = "vb"
    language_name = "VB"
    mapping = (
        u"ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖ×ØÙÚÛÜÝÞßà",
        u"აბგდევზთიკლმნოპჟრსტუფქღყშჩცძწჭხჯჰ",
    )


def print_counter(i):
    sys.stdout.write('\r')
    sys.stdout.write('Processed lines: ' + str(i))
    sys.stdout.flush()


def create(file):
    """
    open a path to a new file, with processed appended in the beginning of existing file name:

    :param file: existing filename
    :returns new file object
    """
    new_file = "processed_" + file
    if os.path.exists(new_file):
        response = input(new_file + " file does already exist, do you want to OVERWRITE it?\nY/N: ").upper()
        if response == "Y":
            pass
        elif response == "N":
            new_file = str(random.getrandbits(32)) + "_" + new_file
        else:
            print("Not a proper response, try again!")
            sys.exit()
    return open(new_file, mode="w", encoding="utf-8")


def process(file):
    """
    Read and process large data files efficiently.
    -> Takes data file and writes to a new identical file: processed + file name.
    :param file, source file
    :returns new file name"""
    new_file = create(file)
    i = 0
    with open(file, encoding="ISO-8859-1") as f:
        for line in f:
            processed_line = (translit(line, 'vb'))
            new_file.write(processed_line)
            i += 1
            print_counter(i)
    new_file.close()
    print("\nData conversion has been completed successfully!")
    return new_file.name



if __name__ == "__main__":
    registry.register(VB)

    args = sys.argv
    if len(args) < 2:
        print("please provide a file name that's ought to be processed.")
        sys.exit()
    path = args[1]
    if os.path.exists(path):
        process(path)
    else:
        print("file doesn't exist, please provide a proper path for an existing file.")
